FROM python:3.9.16-slim
WORKDIR /
COPY requirements.txt .
COPY main.py .
RUN pip install -r requirements.txt
# ENV REDIS_HOST="myredis"
# ENV POSTGRES_PASSWORD="admin"
# ENV POSTGRES_USER="admin"
# ENV POSTGRES_DB="test"
# ENV POSTGRES_HOST="mypg"
CMD [ "uvicorn","main:app","--host","0.0.0.0","--port","8010" ]

