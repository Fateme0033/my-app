from fastapi import FastAPI
import os
import redis
import psycopg2


def get_redis() -> redis.Redis:
    red: redis.Redis = redis.Redis.from_url(url=f"redis://{os.environ['REDIS_HOST']}", decode_responses=True)
    red.ping()
    print('connected to redis')


def get_db():
    conn = psycopg2.connect(
    host=os.environ['POSTGRES_HOST'],
    database=os.environ['POSTGRES_DB'],
    user=os.environ['POSTGRES_USER'],
    password=os.environ['POSTGRES_PASSWORD'],
    )
    cur = conn.cursor()
    print('PostgreSQL database version:')
    cur.execute('SELECT version()')
    db_version = cur.fetchone()
    print(db_version)
    cur.close()

get_redis()
get_db()
app = FastAPI()
print("I am Running")

@app.get("/status")
async def root():
    return {"message": "Project is running"}